<?php
require_once("../function/database.php");
session_start();

$studentid = $_POST['id'];
$name = $_POST['name'];
$level = $_POST['level'];
$from = mysql_real_escape_string($_POST['fromDate']);
$to = mysql_real_escape_string($_POST['toDate']);

if (isset($_POST['tuition'])) {
    $tuition = $_POST['tuition'];
} else {
    $tuition = 0;
}

if (isset($_POST['reg'])) {
    $reg = $_POST['reg'];
} else {
    $reg = 0;
}

if (isset($_POST['print'])) {
    $print = $_POST['print'];
} else {
    $print = 0;
}

$other = $_POST['other'];

$total = $tuition + $reg + $print + $other;

$query = "INSERT INTO Bill ( BillID, Amount, PaymentDate, FeeFrom, FeeTo, Student_StudentID)
            VALUES ( NULL, '$total', Now(), STR_TO_DATE('$from', '%d/%m/%Y'), STR_TO_DATE('$to', '%d/%m/%Y'), '$studentid')";
mysql_query($query);


$printDetail = $level . " on " . date('d/m/Y');

if ($print == 0) {
    $query = "UPDATE Student SET 
            LastPayment = Now()
            WHERE StudentID='$studentid'";
    mysql_query($query);
} else {
    $query = "UPDATE Student SET 
            PrintingFee ='$printDetail',
            LastPayment = Now()
            WHERE StudentID='$studentid'";
    mysql_query($query);
}

function getReceiptNo() {
    $sql_query = mysql_query("SELECT * FROM Bill ORDER BY BillID DESC LIMIT 1");
    $row = mysql_fetch_array($sql_query);
    echo $row['BillID'];
}

require_once("../function/closeDbConn.php");
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pusat Tuisyen Gaya</title>
        <link rel="license" href="http://www.opensource.org/licenses/mit-license/">
        <script src="../js/receipt.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/receipt.css" />
        <script>
            function printFunction() {
                window.print();
            }
        </script>
    </head>
    <body onload="printFunction()">
        <div>
            <header>
                <h1>Pusat Tuisyen Gaya</h1>
                <h5>IP0354853 - A</h5>
                <address contenteditable>
                    <p>109A, Jalan Pengkalan Barat, Taman Shatin Baru,</p>
                    <p>31650 Ipoh, Perak.</p>
                    <p>Tel No: 05-3211502 Fax No: 05-3211502</p>
                </address>
                <h3>RECEIPT</h3>
            </header>
            <article>
                <table class="recipient">
                    <tr>
                        <th>Student ID</th>
                        <td><?php echo $studentid; ?></td>
                    </tr>
                    <tr>
                        <th>Studen Name</th>
                        <td><?php echo $name . " (" . $level . ")"; ?></td>
                    </tr>
                    <tr>
                        <th>Month/Year (From)</th>
                        <td><?php echo $from; ?></td>
                    </tr>
                    <tr>
                        <th>Month/Year (To)</th>
                        <td><?php echo $to; ?></td>
                    </tr>
                </table>
                <table class="meta">
                    <tr>
                        <th>Receipt No:</th>
                        <td><?php getReceiptNo(); ?></td>
                    </tr>
                    <tr>
                        <th>Payment Date</th>
                        <td><?php echo date('d/m/Y'); ?></td>
                    </tr>
                    <tr>
                        <th>Created by</th>
                        <td><?php echo $_SESSION['user']; ?></td>
                    </tr>
                </table>
                <table class="description">
                    <thead>
                        <tr>
                            <th>Tuition Fee (RM)</th>
                            <th>Registration Fee (RM)</th>
                            <th>Printing Fee (RM)</th>
                            <th>Other (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo $tuition; ?></td>
                            <td><?php echo $reg; ?></td>
                            <td><?php echo $print; ?></td>
                            <td><?php echo $other; ?></td>
                        </tr>
                    </tbody>
                </table>
                <table class="total">
                    <tr>
                        <th>Total</th>
                        <td><span data-prefix>RM</span><?php echo $total; ?></td>
                    </tr>
                </table>
            </article>
            <aside>
                <p><i>This is a computer generated document. Authorize signature is not required.</i></p>
            </aside>
        </div>

        <br/><hr/><br/>

        <div>
            <header>
                <h1>Pusat Tuisyen Gaya</h1>
                <h5>IP0354853 - A</h5>
                <address contenteditable>
                    <p>109A, Jalan Pengkalan Barat, Taman Shatin Baru,</p>
                    <p>31650 Ipoh, Perak.</p>
                    <p>Tel No: 05-3211502 Fax No: 05-3211502</p>
                </address>
                <h3>RECEIPT</h3>
            </header>
            <article>
                <table class="recipient">
                    <tr>
                        <th>Student ID</th>
                        <td><?php echo $studentid; ?></td>
                    </tr>
                    <tr>
                        <th>Studen Name</th>
                        <td><?php echo $name . " (" . $level . ")"; ?></td>
                    </tr>
                    <tr>
                        <th>Month/Year (From)</th>
                        <td><?php echo $from; ?></td>
                    </tr>
                    <tr>
                        <th>Month/Year (To)</th>
                        <td><?php echo $to; ?></td>
                    </tr>
                </table>
                <table class="meta">
                    <tr>
                        <th>Receipt No:</th>
                        <td><?php getReceiptNo(); ?></td>
                    </tr>
                    <tr>
                        <th>Payment Date</th>
                        <td><?php echo date('d/m/Y'); ?></td>
                    </tr>
                    <tr>
                        <th>Created by</th>
                        <td><?php echo $_SESSION['user']; ?></td>
                    </tr>
                </table>
                <table class="description">
                    <thead>
                        <tr>
                            <th>Tuition Fee (RM)</th>
                            <th>Registration Fee (RM)</th>
                            <th>Printing Fee (RM)</th>
                            <th>Other (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo $tuition; ?></td>
                            <td><?php echo $reg; ?></td>
                            <td><?php echo $print; ?></td>
                            <td><?php echo $other; ?></td>
                        </tr>
                    </tbody>
                </table>
                <table class="total">
                    <tr>
                        <th>Total</th>
                        <td><span data-prefix>RM</span><?php echo $total; ?></td>
                    </tr>
                </table>
            </article>
            <aside>
                <p><i>This is a computer generated document. Authorize signature is not required.</i></p>
            </aside>
        </div>

    </body>
</html>