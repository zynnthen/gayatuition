<?php
if (!isset($_GET['id'])) {
    header("location:home.php?f=course&loc=searchSubject");
}

$id = $_GET['id'];
$query = "SELECT * FROM Subject WHERE SubjectID = '" . $id . "'  ";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

function getDefaultType() {
    $selected = checkType('Primary');
    echo "<option value='Primary' $selected >Primary</option>";
    $selected = checkType('Secondary');
    echo "<option value='Secondary' $selected >Secondary</option>";
    $selected = checkType('nil');
    echo "<option value='nil' $selected >nil</option>";
}

function checkType($type) {
    $subject_query = mysql_query("SELECT * FROM Subject Where SubjectID='" . $_GET['id'] . "' AND Type ='" . $type . "'");
    $num_row = mysql_num_rows($subject_query);
    if ($num_row == 1) {
        return 'selected';
    }
}

function checkPackage() {
    if ($_GET['p'] == "Yes") {
        include("editPackage.php");
    }
}

session_start();

function msg() {
    if (isset($_SESSION['Edit.Msg'])) {
        echo "<script>";
        echo "alert('Successfully updated')";
        echo "</script>";
        unset($_SESSION['Edit.Msg']);
    }
}
?>

<div id="right">
    <a href="?f=course&loc=searchSubject">Back to Subject/Package List</a>
    <h2>Edit Subject/Package</h2>

    <form id="newSubjectForm" method="post" action="../widget/editSubject_bg.php">
        <table>
            <input type="hidden" id="id" name="id"  value="<?php echo $row[SubjectID]; ?>">
            <input type="hidden" id="package" name="package"  value="<?php echo $row[Package]; ?>">
            <tr>
                <td>Name</td>
                <td><input type="text" id="name" name="name" required autofocus value="<?php echo $row[Description]; ?>"></td>
            </tr>
            <tr>
                <td>Type</td>
                <td>
                    <select id="type" name="type" required>
                        <option value=""></option>
                        <?php getDefaultType(); ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Fee</td>
                <td>RM<input type='number' step="any" id='fee' name="fee" required value="<?php echo $row[Price]; ?>" /></td>
            </tr>
        </table>

        <?php checkPackage(); ?>

        <input type="submit" value="Save" onclick="return confirm('Confirm Save?')" />
    </form>

    <?php msg(); ?>
</div>
