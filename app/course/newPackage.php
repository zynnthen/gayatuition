<?php

function querySubject($str) {
    $query = mysql_query("SELECT * FROM Subject Where Type='$str' AND Package IS NULL");

    while ($info = mysql_fetch_assoc($query)) {
        echo "<tr><td>";
        echo "<input type='checkbox' id='{$info['SubjectID']}' name='subject[]' value='{$info['SubjectID']}' >";
        echo $info['Description'];
        echo "</td></ tr>";
    }
}

session_start();
function msg() {
    if (isset($_SESSION['Add.Msg'])) {
        echo "<script>";
        echo "alert('Successfully inserted')";
        echo "</script>";
        unset($_SESSION['Add.Msg']);
    }
}
?>

<div id="right">
    <h2>New Package</h2>

    <form id="newPackageForm" method="post" action="../widget/newPackage_bg.php">
        <table>
            <tr>
                <td>Package Name</td>
                <td><input type="text" id="name" name="name" required autofocus></td>
            </tr>
            <tr>
                <td>Type</td>
                <td>
                    <select id="type" name="type" required>
                        <option value=""></option>
                        <option value="Primary">Primary</option>
                        <option value="Secondary">Secondary</option>
                        <option value="nil">N/A</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Fee</td>
                <td>RM<input type='number' step="any" id='fee' name="fee" required /></td>
            </tr>
        </table>

        <h3>Subject</h3>
        <div class="fill">
            <table class="fill"  style="width: 70%;">
                <tr>
                    <th class="fill">Primary</th>
                    <th class="fill">Secondary</th>
                    <th class="fill">Other</th>
                </tr>

                <tr>
                    <td class="fill" valign = "top">
                        <table style="none">
                            <?php querySubject("Primary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Secondary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("nil"); ?>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <input type="submit" value="Save" />
    </form>
    
    <?php msg(); ?>
</div>
