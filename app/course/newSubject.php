<?php
session_start();

function msg() {
    if (isset($_SESSION['Add.Msg'])) {
        echo "<script>";
        echo "alert('Successfully inserted')";
        echo "</script>";
        unset($_SESSION['Add.Msg']);
    }
}
?>

<div id="right">
    <h2>New Subject</h2>

    <form id="newSubjectForm" method="post" action="../widget/newSubject_bg.php">
        <table>
            <tr>
                <td>Subject Name</td>
                <td><input type="text" id="name" name="name" required autofocus></td>
            </tr>
            <tr>
                <td>Type</td>
                <td>
                    <select id="type" name="type" required>
                        <option value=""></option>
                        <option value="Primary">Primary</option>
                        <option value="Secondary">Secondary</option>
                        <option value="nil">N/A</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Fee</td>
                <td>RM<input type='number' step="any" id='fee' name="fee" required /></td>
            </tr>
        </table>
        <input type="submit" value="Save" />
    </form>

    <?php msg(); ?>
</div>
