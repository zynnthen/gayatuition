<div id="left">
    
    <ul id="menu">

        <li>Billing
            <ul>
                <li><a href="?f=bill&loc=searchStudent">Student Billing</a></li>
                <li><a href="?f=bill&loc=searchTeacher">Teacher Payment</a>
                    <ul>
                        <li><a href="?f=bill&loc=teacherSummarize">Summarized Teacher Payment</a></li>
                    </ul>
                </li>
            </ul>
        </li>

        <li>Student Profile
            <ul>
                <li><a href="?f=student&loc=newStudent">New Student</a></li>
                <li><a href="?f=student&loc=searchStudent">Search / Edit Student</a></li>
            </ul>
        </li>
        
        <li>Teacher Profile
            <ul>
                <li><a href="?f=teacher&loc=newTeacher">New Teacher</a></li>
                <li><a href="?f=teacher&loc=searchTeacher">Search / Edit Teacher</a></li>
            </ul>
        </li>

        <li>Course
            <ul>
                <li><a href="?f=course&loc=newSubject">New Subject</a></li>
                <li><a href="?f=course&loc=newPackage">New Package</a></li>
                <li><a href="?f=course&loc=searchSubject">Edit Subject / Package</a></li>
            </ul>
        </li>

        <li>Setting
            <ul>
                <li><a href="?f=set&loc=updateLevel">Update Student Level</a></li>
                <li><a href="?f=set&loc=setFee">Set Fees</a></li>
                <li><a href="?f=set&loc=chgPwd">Change Password</a></li>
            </ul>
        </li>

        <li><a href="../widget/logoutAction.php">Log out</a></li>

    </ul>

</div>