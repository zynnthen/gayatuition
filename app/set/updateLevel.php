<?php

function showDate() {
    $query = "SELECT LastUpdate FROM Miscellaneous Limit 1";
    $sql_query = mysql_query($query);
    $row = mysql_fetch_array($sql_query);
    if ($row[LastUpdate] != null) {
        $date = new DateTime($row[LastUpdate]);
        echo $date->format('d/m/Y');
    } else {
        echo "N/A";
    }
}
?>

<div id="right">
    <h2>Update Student Level</h2>

    <form id="updateProfile" action="../widget/updateLevel_bg.php">
        By clicking "update", the system will update all the student's level by one and existing Form 5 students will be removed from the system.<br/>
        Note: Admin may manually edit the student profile.
        <br/><br/>
        Last update: <?php showDate(); ?>
        <br/><br/>
        <input type="submit" value="Update" onclick="return confirm('Are you sure to update?')" />
    </form>
</div>
