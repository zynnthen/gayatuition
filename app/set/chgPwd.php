<?php
session_start();

function msg() {
    if (isset($_SESSION['Error.Msg'])) {
        echo "<font color='red'>";
        echo $_SESSION['Error.Msg'] . "<br/><br/>";
        echo "</font>";
        unset($_SESSION['Error.Msg']);
    } else if (isset($_SESSION['Success.Msg'])) {
        echo "<font color='green'>";
        echo $_SESSION['Success.Msg'] . "<br/><br/>";
        echo "</font>";
        unset($_SESSION['Success.Msg']);
    }
}
?>

<div id="right">
    <h2>Change Password</h2>

    <form id="chgPwdForm" method="post" action="../widget/chgPwd_bg.php">
        <table>
            <tr>
                <td>Old Password :</td>
                <td><input type="password" name='oldPwd' id="oldPwd" required/></td>

            </tr>
            <tr>
                <td>New Password :</td>
                <td><input type="password" name='newPwd1' id="newPwd1" required/></td>
            </tr>
            <tr>
                <td>Retype New Password :</td>
                <td><input type="password" name='newPwd2' id="newPwd2" required/>  </td>
            </tr>
        </table>

        <?php msg(); ?>

        <input type="submit" id="submit" value="Reset Password" onclick="return confirm('Are you sure to change password?')" />
    </form>

</div>
