<?php
if (!isset($_GET['id'])) {
    header("location:home.php?f=bill&loc=searchStudent");
}

$id = $_GET['id'];
$query = "SELECT * FROM Student WHERE StudentID = '" . $id . "'  ";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

function calTuitionFee() {
    $query = "SELECT * FROM StudentSubject WHERE StudentID = '" . $_GET['id'] . "'  ";
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result)) {
        $arr = $arr . ',' . $row['SubjectID'];
    }
    $arr = substr($arr, 1);

    $subject = explode(',', $arr);
    $tuitionFee = "";
    foreach ($subject as $value) {
        $subject_query = mysql_query("SELECT * FROM Subject Where SubjectID = $value");
        $row = mysql_fetch_array($subject_query);
        $tuitionFee = $tuitionFee + $row['Price'];
    }
    echo $tuitionFee;
}

function regFee() {
    $query = "SELECT * FROM Miscellaneous Limit 1";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    echo $row[RegistrationFee];
}

function printFee() {
    $query = "SELECT * FROM Miscellaneous Limit 1";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    echo $row[PrintingFee];
}
?>

<script>
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    $(function() {
        $("#fromDate").datepicker({
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
                var httpxml;
                try {
                    // Firefox, Opera 8.0+, Safari
                    httpxml = new XMLHttpRequest();
                }
                catch (e) {
                    // Internet Explorer
                    try {
                        httpxml = new ActiveXObject("Msxml2.XMLHTTP");
                    }
                    catch (e) {
                        try {
                            httpxml = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        catch (e) {
                            alert("Your browser does not support AJAX!");
                            return false;
                        }
                    }
                }
                function stateChanged() {
                    if (httpxml.readyState == 4) {
                        if (httpxml.responseText > 0) {
                            $("#fromError").html('<span class="error" style="color:red">Invalid date (Duplicate bill)</span>');
                        } else {
                            $("#fromError").html('<span class="error"></span>');
                        }
                    }
                }
                var url = "http://localhost:8080/GayaTuition/app/bill/checkBill.php";
                var type = "to";
                var studentid = getParameterByName('id');
                url = url + "?type=" + type + "&txt=" + dateText + "&id=" + studentid;
                httpxml.onreadystatechange = stateChanged;
                httpxml.open("GET", url, true);
                httpxml.send(null);
            }
        });

        $("#toDate").datepicker({
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
                var httpxml;
                try {
                    // Firefox, Opera 8.0+, Safari
                    httpxml = new XMLHttpRequest();
                }
                catch (e) {
                    // Internet Explorer
                    try {
                        httpxml = new ActiveXObject("Msxml2.XMLHTTP");
                    }
                    catch (e) {
                        try {
                            httpxml = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        catch (e) {
                            alert("Your browser does not support AJAX!");
                            return false;
                        }
                    }
                }
                function stateChanged() {
                    if (httpxml.readyState == 4) {
                        if (httpxml.responseText > 0) {
                            $("#toError").html('<span class="error" style="color:red">Invalid date (Duplicate bill)</span>');
                        } else {
                            $("#toError").html('<span class="error"></span>');
                        }
                    }
                }
                var url = "http://localhost:8080/GayaTuition/app/bill/checkBill.php";
                var type = "to";
                var studentid = getParameterByName('id');
                url = url + "?type=" + type + "&txt=" + dateText + "&id=" + studentid;
                url = url + "?type=" + type + "&txt=" + dateText;
                httpxml.onreadystatechange = stateChanged;
                httpxml.open("GET", url, true);
                httpxml.send(null);
            }
        });
    });

</script>

<div id="right">
    <a href="?f=bill&loc=searchStudent">Back to Student List</a>
    <h2>Student Bill</h2>

    <form id="editStudentForm" method="post" action="../widget/studentBill_bg.php">
        <table>
            <tr>
                <td>Student ID</td>
                <td><input type='label' id='id' name='id' readonly value="<?php echo $row[StudentID]; ?>" /></td>
            </tr>
            <tr>
                <td>Name</td>
                <td><input type="label" id="name" name="name" readonly value="<?php echo $row[Name]; ?>" ></td>
            </tr>
            <tr>
                <td>Level</td>
                <td><input type="label" id="level" name="level" readonly value="<?php echo $row[Level]; ?>" ></td>
            </tr>
            <tr>
                <td>Last Printing Fee</td>
                <td><input type="label" id="last_print" name="last_print" readonly value="<?php echo $row[PrintingFee]; ?>" ></td>
            </tr>
        </table>

        <hr/>
        <h3>Bill History</h3>
        <?php include("billHistory.php"); ?>
        <hr/>

        <table>
            <tr>
                <td>From</td>
                <td>
                    <input type='text' name='fromDate' id="fromDate" required />
                </td>
                <td>
                    <div class="fromError" id="fromError"></div>
                </td>
            </tr>
            <tr>
                <td>To</td>
                <td>
                    <input type='text' name='toDate' id="toDate" required />
                </td>
                <td>
                    <div class="toError" id="toError"></div>
                </td>
            </tr>
            <tr>
                <td>Tuition Fee</td>
                <td>
                    <input type='checkbox' id='tuition' name='tuition' value='<?php calTuitionFee(); ?>' />
                    RM <?php calTuitionFee(); ?>
                </td>
            </tr>
            <tr>
                <td>Registration Fee</td>
                <td>
                    <input type='checkbox' id='reg' name='reg' value='<?php regFee(); ?>' />
                    RM <?php regFee(); ?>
                </td>
            </tr>
            <tr>
                <td>Printing Fee</td>
                <td>
                    <input type='checkbox' id='print' name='print' value='<?php printFee(); ?>' />
                    RM <?php printFee(); ?>
                </td>
            </tr>
            <tr>
                <td>Other</td>
                <td>
                    RM<input type='number' step="any"  name='other' id='other' />
                </td>
            </tr>
        </table>

        <input type="submit" value="Save and Print" onclick="return confirm('Confirm save and print?')" />
    </form>

</div>