<?php
if (!isset($_GET['id'])) {
    header("location:home.php?f=bill&loc=searchStudent");
}

$id = $_GET['id'];
$query = "SELECT * FROM Teacher WHERE TeacherID = '" . $id . "'  ";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

function calPayment() {
    $payment = "";
    $totalpayment = "";
    $percentage = "";

    // get teaching subject
    $query = "SELECT * FROM TeacherSubject WHERE TeacherID = '" . $_GET['id'] . "'  ";
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result)) {
        $arr = $arr . ',' . $row['SubjectID'];
    }
    $arr = substr($arr, 1);
    $subject = explode(',', $arr);

    // get package array
    $query = "SELECT * FROM Subject WHERE Package = 'Yes'  ";
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result)) {
        $packageID = $row['SubjectID'];
        $arr = $row['PackageDesc'];
    }
    $packageSubject = explode(',', $arr);

    foreach ($subject as $value) {
        // obtain subject tuition fee
        $subject_fee_query = mysql_query("SELECT * FROM Subject Where SubjectID = $value");
        $row = mysql_fetch_array($subject_fee_query);
        $subjectName = $row['Description'];
        $subjectFee = $row['Price'];

        // calculate student amount (non-package)
        $student_amount_query = "select * from StudentSubject where SubjectID=$value";
        $result = mysql_query($student_amount_query);
        $num_row_non_package = mysql_num_rows($result);
        
        // calculate student amount (package)
        foreach ($packageSubject as $packageValue) {
            if ($packageValue == $value) {
                $student_amount_query = "select * from StudentSubject where SubjectID=$packageID";
                $result = mysql_query($student_amount_query);
                $num_row_package = mysql_num_rows($result);
            }
        }
        
        // total number of student;
        $num_row = $num_row_non_package + $num_row_package;

        if (stripos($subjectName, "Piano") !== false) {
            $percentage = 0.8;
        } else if (stripos($subjectName, "BC") !== false) {
            $percentage = 0.7;
        } else {
            $percentage = 0.6;
        }

        $payment = ($subjectFee * $percentage) * $num_row;
        $totalpayment = $totalpayment + $payment;
    }
    echo $totalpayment;
}

function epfAmount() {
    $query = "SELECT * FROM Miscellaneous Limit 1";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    echo $row[EPF];
}
?>

<div id="right">
    <a href="?f=bill&loc=searchTeacher">Back to Teacher List</a>
    <h2>Teacher Payment</h2>

    <form id="teacherPaymentForm" method="post" action="../widget/teacherPayment_bg.php">
        <table>
            <input type="hidden" id='id' name='id' value="<?php echo $row[TeacherID]; ?>">
            <tr>
                <td>Name</td>
                <td><input type="label" id="name" name="name" readonly value="<?php echo $row[Name]; ?>" ></td>
            </tr>
        </table>

        <hr/>
        <h3>Payment History</h3>
        <?php include("paymentHistory.php"); ?>
        <hr/>

        <table>
            <tr>
                <td>Payment For</td>
                <td>
                    <select name="month" required>
                        <option value="">-- Month --</option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                    <input type='number' name='year' id='year' value="<?php echo date('Y'); ?>" required />
                </td>
            </tr>
            <tr>
                <td>Payment</td>
                <td>
                    <input type='checkbox' id='pay' name='pay' value='<?php calPayment(); ?>' />
                    RM <?php calPayment(); ?>
                </td>
            </tr>
            <tr>
                <td>EPF</td>
                <td>
                    <input type='checkbox' id='epf' name='epf' value='<?php epfAmount(); ?>' />
                    RM <?php epfAmount(); ?>
                </td>
            </tr>
            <tr>
                <td>Other</td>
                <td>
                    RM<input type='number' step="any"  name='other' id='other' />
                </td>
            </tr>
        </table>

        <input type="submit" value="Save" onclick="return confirm('Confirm save?')" />
    </form>

</div>
