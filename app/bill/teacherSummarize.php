<?php
$host = 'localhost';
$username = 'root';
$password = 'root';
$db_name = "mydb";

// Connect to server and select database.
mysql_connect("$host", "$username", "$password") or die(mysql_error());
mysql_select_db("$db_name") or die(mysql_error());

// $month = intval($_GET['month']);
// $year = intval($_GET['year']);

$teacherTotal = 0;
$centerTotal = 0;
$packageID = 0;

// get student amount for each subject
function studentAmount($subject, $type) {
    // get student amount for non-package sign up student
    $query = "SELECT * FROM StudentSubject WHERE subjectid='" . $subject . "'";
    $result1 = mysql_query($query);
    $num_row1 = mysql_num_rows($result1);

    // get student amount with signing up package by checking with the $subject
    $query = "SELECT * FROM Subject Where packagedesc LIKE '%" . $subject . "%' ";
    $result2 = mysql_query($query);
    $row = mysql_fetch_array($result2);
    $query = "SELECT * FROM StudentSubject WHERE subjectid='" . $row['SubjectID'] . "'"; // the package subject id
    // store packageId as global variable for calCenter()
    $GLOBALS['packageID'] = $row['SubjectID'];
    $result3 = mysql_query($query);
    $num_row2 = mysql_num_rows($result3);

    if (stripos($type, "teacher") !== false) {
        return $num_row1 + $num_row2;
    } else if (stripos($type, "centerNonPackage") !== false) {
        return $num_row1;
    } else {
        return $num_row2;
    }
}

function calTeacher($subject) {
    $type = "teacher";
    $studentAmount = studentAmount($subject, $type);

    //get total income
    $query = "SELECT * FROM Subject WHERE subjectid='" . $subject . "'";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    $price = $row['Price'];

    if (stripos($row['Description'], "Piano") !== false) {
        $percentage = 0.8;
    } else if (stripos($row['Description'], "BC") !== false) {
        $percentage = 0.7;
    } else {
        $percentage = 0.6;
    }

    $teacherIncome = $studentAmount * $price * $percentage;
    $GLOBALS['teacherTotal'] = $GLOBALS['teacherTotal'] + $teacherIncome;
    echo $teacherIncome;
}

function calCenter($subject) {
    $query = "SELECT * FROM Subject WHERE subjectid='" . $subject . "'";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);

    if (stripos($row['Description'], "Piano") !== false) {
        $percentage = 0.2;
    } else if (stripos($row['Description'], "BC") !== false) {
        $percentage = 0.3;
    } else {
        $percentage = 0.4;
    }

    //get total income (non-package)
    $type = "centerNonPackage";
    $studentAmount1 = studentAmount($subject, $type);
    $price = $row['Price']; // normal price
    $centerIncome1 = $studentAmount1 * $price * $percentage;

    // get discounted price for each subject
    $query = "SELECT * FROM Subject WHERE subjectid='" . $GLOBALS['packageID'] . "'";
    $resultPackageArr = mysql_query($query);
    $rowPackage = mysql_fetch_array($resultPackageArr);
    $arr = $rowPackage['PackageDesc'];
    $packagePrice = $rowPackage['Price'];
    $packageSubject = explode(',', $arr);
    $packageSubNum = count($packageSubject);
    $avgPackagePrice = $packagePrice / $packageSubNum; //avg 1 subject price
    $discountPrice = $price - $avgPackagePrice;

    //get total income (package)
    $type = "centerPackage";
    $studentAmount2 = studentAmount($subject, $type);
    $centerIncome2 = $studentAmount2 * ($price - $discountPrice) * $percentage;
    $GLOBALS['centerTotal'] = $GLOBALS['centerTotal'] + $centerIncome1 + $centerIncome2;
    echo $centerIncome1 + $centerIncome2;
}

function calTeacherTotal() {
    echo $GLOBALS['teacherTotal'];
}

function calCenterTotal() {
    echo $GLOBALS['centerTotal'];
}

function excelFileName() {
    echo date("m-Y") . "_GayaTeacherPayment";
}
?>

<script type="text/javascript">
    function printDiv(divID) {
        // get Month
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";

        var d = new Date();
        var m = month[d.getMonth()];
        var y = d.getFullYear();

        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;
        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
                "<html><head>" +
                "<title>Pusat Tuisyen Gaya</title>" +
                "<style>@media print{ table {border-collapse: collapse; border-spacing:0 5px; width: 90%; }\n\
                    table td, table th { border-collapse: collapse; border: 1px solid black;}\n\
                }</style>" +
                "</head>" +
                "<body>" +
                "<h3>Teacher Payment Report: " + m + "/" + y + "</h3>" +
                divElements + "</body>";
        //Print Page
        window.print();
        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }
</script>

<style>
    table {
        border-collapse: collapse;
        border-spacing:0 5px;
        width: 90%;
    }

    table, td, th {
        border: 1px solid black;
        font-family:sans-serif;
        font-size:15pt;
    }

    tr#element:hover{
        background-color: aquamarine;
        cursor: pointer;
    }
</style>

<div id="right">
    <h2>Summarized Payout</h2>

    <ul style="list-style-type: none; margin: 0; padding: 0;">
        <li style="display: inline; padding: 0 10px;">
            <a href="#" onclick="javascript:printDiv('result')">
                <img src='../images/print.png' name='edit' width='20' height='20'/>
            </a>
        </li>
        <li style="display: inline; padding: 0 10px;">
            <a download="<?php excelFileName(); ?>" href="#" onclick="return ExcellentExport.excel(this, 'datatable', 'Sheet A');">
                <img src='../images/excel.png' name='edit' width='20' height='20'/>
            </a>
        </li>
    </ul>

    <div id="result">
        <table id="datatable">
            <th>Subject</th>
            <th>Type</th>
            <th>Teacher (RM)</th>
            <th>Center (RM)</th>

            <?php
            $query = "SELECT * FROM Subject WHERE Package IS NULL ORDER BY Type, Description  ";
            $result = mysql_query($query);

            while ($row = mysql_fetch_array($result)) {
                ?>
                <tr id="element">
                    <td> <?php echo $row['Description']; ?> </td>
                    <td> <?php echo $row['Type']; ?> </td>
                    <td align="center"> <?php calTeacher($row['SubjectID']); ?> </td>
                    <td align="center"> <?php calCenter($row['SubjectID']); ?> </td>
                </tr>
            <?php } ?>

            <tr>
                <td colspan="2" align="center" bgcolor="#FFFBD0"><font size="6">Total</font></td>
                <td align="center" bgcolor="#FFFBD0"><font size="6" color="blue"><?php calTeacherTotal(); ?></font></td>
                <td align="center" bgcolor="#FFFBD0"><font size="6" color="blue"><?php calCenterTotal(); ?></font></td>
            </tr>
        </table>
    </div>

</div>
