<?php
$host = 'localhost';
$username = 'root';
$password = 'root';
$db_name = "mydb";

// Connect to server and select database.
mysql_connect("$host", "$username", "$password") or die(mysql_error());
mysql_select_db("$db_name") or die(mysql_error());

$tableName = "bill";
$id = $_GET['id'];
$targetpage = "http://localhost/GayaTuition/app/home.php?f=bill&loc=studentBill&id=$id";
$limit = 5;

$num = 'num';

$query = "SELECT COUNT(*) as $num FROM $tableName WHERE Student_StudentID=$id ";
$total_result = mysql_fetch_array(mysql_query($query));
$total_result = $total_result[$num];

$stages = 3;
if (!isset($_GET['page'])) {
    $page = '1';
} else {
    $page = mysql_real_escape_string($_GET['page']);
}
if ($page) {
    $start = ($page - 1) * $limit;
} else {
    $start = 0;
}

$query = "SELECT * FROM $tableName WHERE Student_StudentID=$id ORDER BY PaymentDate Desc LIMIT $start, $limit";

// Initial page num setup
if ($page == 0) {
    $page = 1;
}
$prev = $page - 1;
$next = $page + 1;
$lastpage = ceil($total_result / $limit);
$LastPagem1 = $lastpage - 1;


$paginate = '';
if ($lastpage > 1) {

    $paginate .= "<div class='paginate'>";
    // Previous
    if ($page > 1) {
        $paginate.= "<a href='$targetpage&page=$prev'>previous</a>";
    } else {
        $paginate.= "<span class='disabled'>previous</span>";
    }

    // Pages	
    if ($lastpage < 7 + ($stages * 2)) { // Not enough pages to breaking it up
        for ($counter = 1; $counter <= $lastpage; $counter++) {
            if ($counter == $page) {
                $paginate.= "<span class='current'>$counter</span>";
            } else {
                $paginate.= "<a href='$targetpage&page=$counter'>$counter</a>";
            }
        }
    } else if ($lastpage > 5 + ($stages * 2)) { // Enough pages to hide a few?
        // Beginning only hide later pages
        if ($page < 1 + ($stages * 2)) {
            for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
                if ($counter == $page) {
                    $paginate.= "<span class='current'>$counter</span>";
                } else {
                    $paginate.= "<a href='$targetpage&page=$counter'>$counter</a>";
                }
            }
            $paginate.= "...";
            $paginate.= "<a href='$targetpage&page=$LastPagem1'>$LastPagem1</a>";
            $paginate.= "<a href='$targetpage&page=$lastpage'>$lastpage</a>";
        }
        // Middle hide some front and some back
        else if ($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
            $paginate.= "<a href='$targetpage&page=1'>1</a>";
            $paginate.= "<a href='$targetpage&page=2'>2</a>";
            $paginate.= "...";
            for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
                if ($counter == $page) {
                    $paginate.= "<span class='current'>$counter</span>";
                } else {
                    $paginate.= "<a href='$targetpage&page=$counter'>$counter</a>";
                }
            }
            $paginate.= "...";
            $paginate.= "<a href='$targetpage&page=$LastPagem1'>$LastPagem1</a>";
            $paginate.= "<a href='$targetpage&page=$lastpage'>$lastpage</a>";
        }
        // End only hide early pages
        else {
            $paginate.= "<a href='$targetpage&page=1'>1</a>";
            $paginate.= "<a href='$targetpage&page=2'>2</a>";
            $paginate.= "...";
            for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
                if ($counter == $page) {
                    $paginate.= "<span class='current'>$counter</span>";
                } else {
                    $paginate.= "<a href='$targetpage&page=$counter'>$counter</a>";
                }
            }
        }
    }

    // Next
    if ($page < $counter - 1) {
        $paginate.= "<a href='$targetpage&page=$next'>next</a>";
    } else {
        $paginate.= "<span class='disabled'>next</span>";
    }

    $paginate.= "</div>";
}

// pagination
echo $paginate;

function displayDate($date) {
    $date = new DateTime($date);
    echo $date->format('d/m/Y');
}
?>

<style>
    table#history {
        border-collapse: collapse;
        border-spacing:0 5px;
        width: 90%;
    }

    table.historyBill, td.historyBill, th.historyBill {
        border: 1px solid black;
        font-family:sans-serif;
        font-size:15pt;
    }

    tr#element:hover{
        background-color: aquamarine;
        cursor: pointer;
    }
</style>

<table id="history" class="historyBill">
    <th class="historyBill">Payment Date</th>
    <th class="historyBill">From</th>
    <th class="historyBill">To</th>
    <th class="historyBill">Amount (RM)</th>

    <?php
    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result)) {
        ?>
        <tr id="element">
            <td class="historyBill" align="center"><?php displayDate($row['PaymentDate']); ?></td>
            <td class="historyBill"><font face="Comic Sans MS" color="blue"> <?php displayDate($row['FeeFrom']); ?> </font></td>
            <td class="historyBill"><font face="Comic Sans MS" color="blue"> <?php displayDate($row['FeeTo']); ?> </font></td>
            <td class="historyBill"> <?php echo $row['Amount']; ?> </td>
        </tr>
    <?php } ?>
</table>