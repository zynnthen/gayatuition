<script type="text/javascript">    
    function showResult(str){
        var httpxml;
        try{
            // Firefox, Opera 8.0+, Safari
            httpxml=new XMLHttpRequest();
        }
        catch (e){
            // Internet Explorer
            try{
                httpxml=new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e){
                try{
                    httpxml=new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e){
                    alert("Your browser does not support AJAX!");
                    return false;
                }
            }
        }
        function stateChanged() {
            if(httpxml.readyState==4){
                document.getElementById("result").innerHTML=httpxml.responseText;
            }
        }
        var url="http://localhost/GayaTuition/app/student/searchStudent_bg.php";
        url=url+"?txt="+str;
        httpxml.onreadystatechange=stateChanged;
        httpxml.open("GET",url,true);
        httpxml.send(null);
    }
</script>

<style>
    table {
        border-collapse: collapse;
        border-spacing:0 5px;
        width: 90%;
    }

    table, td, th {
        border: 1px solid black;
        font-family:sans-serif;
        font-size:15pt;
    }

    tr#element:hover{
        background-color: aquamarine;
        cursor: pointer;
    }
</style>

<div id="right">
    <h2>Student List</h2>

    <form>
        Name:
        <input type="text" id="searchid" placeholder="Search with name" onkeyup="showResult(this.value)">
    </form>

    <div id="result">
        <?php include("searchStudent_bg.php"); ?>
    </div>

</div>
