<?php

function querySubject($str) {
    $query = mysql_query("SELECT * FROM Subject Where Type='$str' ORDER BY Package Desc");

    while ($info = mysql_fetch_assoc($query)) {
        echo "<tr><td>";
        echo "<input type='checkbox' id='{$info['SubjectID']}' name='subject[]' value='{$info['SubjectID']}' >";
        echo $info['Description'];
        echo "</td></ tr>";
    }
}

session_start();

function msg() {
    if (isset($_SESSION['Add.Msg'])) {
        echo "<script>";
        echo "alert('Successfully inserted')";
        echo "</script>";
        unset($_SESSION['Add.Msg']);
    }
}
?>

<script>
    function getClass()
    {
        // clear the classes drop down list
        var select = document.getElementById("class");
        for (var i = select.options.length - 1; i >= 0; i--)
        {
            select.remove(i);
        }

        var arr = new Array("1", "2", "3", "4", "5", "6");

        var level = document.getElementById("level").value;
        var classes = document.getElementById("class");
        if (level == "Primary") {
            for (var i = 1; i < arr.length + 1; ++i) {
                var option = document.createElement('option');
                option.text = option.value = i;
                classes.add(option, 0);
            }
        }
        else if (level == "Secondary") {
            for (var i = 1; i < arr.length; ++i) {
                var option = document.createElement('option');
                option.text = option.value = i;
                classes.add(option, 0);
            }
        }
    }

</script>

<div id="right">
    <h2>New Student</h2>

    <form id="newStudentForm" method="post" action="../widget/newStudent_bg.php">
        <h3>Student Particular</h3>
        <div class="fill">
            <table>
                <tr>
                    <td>Name</td>
                    <td><input type="text" id="name" name="name" required autofocus></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td>
                        <select id="gender" name="gender" required>
                            <option value=""></option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Race</td>
                    <td>
                        <select id="race" name="race" required>
                            <option value=""></option>
                            <option value="Malay">Malay</option>
                            <option value="Chinese">Chinese</option>
                            <option value="India">India</option>
                            <option value="Bumiputra">Bumiputra</option>
                            <option value="Other">Other</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Contact (Home)</td>
                    <td><input type="text" id="home" name="home"></td>
                </tr>
                <tr>
                    <td>Contact (Mobile)</td>
                    <td><input type="text" id="mobile" name="mobile"></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><textarea id="address" name="address" form="newStudentForm" rows="5" cols="50"></textarea></td>
                </tr>
                <tr>
                    <td>Level</td>
                    <td>
                        <select id="level" name="level" required  onchange="getClass();">
                            <option value=""></option>
                            <option value="Primary">Primary</option>
                            <option value="Secondary">Secondary</option>
                            <option value="nil">N/A</option>
                        </select>
                        <select id="class" name="class">
                        </select>
                    </td>
                </tr>
            </table>
        </div>

        <h3>Parent/Guardian Particular</h3>
        <div class="fill">
            <table>
                <tr>
                    <td>Name</td>
                    <td><input type="text" id="parentName" name="parentName"></td>
                </tr>
                <tr>
                    <td>Contact (Mobile)</td>
                    <td><input type="text" id="parentPhone" name="parentPhone"></td>
                </tr>
            </table>
        </div>

        <h3>Subject</h3>
        <div class="fill">
            <table class="fill"  style="width: 70%;">
                <tr>
                    <th class="fill">Primary</th>
                    <th class="fill">Secondary</th>
                    <th class="fill">Other</th>
                </tr>

                <tr>
                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Primary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Secondary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("nil"); ?>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <input type="submit" value="Save" onclick="return confirm('Confirm Save?')" />
    </form>

    <?php msg(); ?>
</div>
