<?php
if (!isset($_GET['id'])) {
    header("location:home.php?f=student&loc=searchStudent");
}

$studentid = $_GET['id'];
$query = "SELECT * FROM Student WHERE StudentID = '" . $studentid . "'  ";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

function querySubject($str) {
    $query = mysql_query("SELECT * FROM Subject Where Type='$str' ORDER BY Package Desc");

    while ($info = mysql_fetch_assoc($query)) {
        $checked = checkRegSubject($info['SubjectID']);
        echo "<tr><td>";
        echo "<input type='checkbox' id='{$info['SubjectID']}' name='subject[]' value='{$info['SubjectID']}' $checked >";
        echo $info['Description'];
        echo "</td></ tr>";
    }
}

function checkRegSubject($subjectID) {
    $student_subject_query = mysql_query("SELECT * FROM StudentSubject Where StudentID='" . $_GET['id'] . "' AND SubjectID = $subjectID");
    $num_row = mysql_num_rows($student_subject_query);
    if ($num_row == 1) {
        return 'checked';
    }
}

function getDefaultClass() {
    $query = "SELECT * FROM Student WHERE StudentID = '" . $_GET['id'] . "'  ";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    $level = $row[Level];
    $class = substr($row[Level], -1);

    $arr = array("1", "2", "3", "4", "5", "6");
    $arrlength = count($arr);
    if (strpos($level, 'Primary') !== false) {
        for ($i = 0; $i < $arrlength; ++$i) {
            $display_class = $i + 1;
            if ($display_class == $class) {
                echo "<option value='$display_class' selected >$display_class</option>";
            } else {
                echo "<option value='$display_class' >$display_class</option>";
            }
        }
    } else if (strpos($level, 'Secondary') !== false) {
        for ($i = 0; $i < $arrlength - 1; ++$i) {
            $display_class = $i + 1;
            if ($display_class == $class) {
                echo "<option value='$display_class' selected >$display_class</option>";
            } else {
                echo "<option value='$display_class' >$display_class</option>";
            }
        }
    }
}

function getDefaultGender() {
    $query = "SELECT * FROM Student WHERE StudentID = '" . $_GET['id'] . "'  ";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    $gender = $row[Gender];

    if (strcasecmp($gender, 'Male') == 0) {
        echo "<option value='Male' selected >Male</option>";
        echo "<option value='Female' >Female</option>";
    } else if (strcasecmp($gender, 'Female') == 0) {
        echo "<option value='Male' >Male</option>";
        echo "<option value='Female' selected>Female</option>";
    }
}

function getDefaultRace() {
    $query = "SELECT * FROM Student WHERE StudentID = '" . $_GET['id'] . "'  ";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    $race = $row[Race];

    if (strcasecmp($race, 'Malay') == 0) {
        echo "<option value='Malay' selected >Malay</option>";
        echo "<option value='Chinese' >Chinese</option>";
        echo "<option value='India' >India</option>";
        echo "<option value='Bumiputra' >Bumiputra</option>";
        echo "<option value='Other' >Other</option>";
    } else if (strcasecmp($race, 'Chinese') == 0) {
        echo "<option value='Malay' >Malay</option>";
        echo "<option value='Chinese' selected >Chinese</option>";
        echo "<option value='India' >India</option>";
        echo "<option value='Bumiputra' >Bumiputra</option>";
        echo "<option value='Other' >Other</option>";
    } else if (strcasecmp($race, 'India') == 0){
        echo "<option value='Malay' >Malay</option>";
        echo "<option value='Chinese' >Chinese</option>";
        echo "<option value='India' selected>India</option>";
        echo "<option value='Bumiputra' >Bumiputra</option>";
        echo "<option value='Other' >Other</option>";
    } else if (strcasecmp($race, 'Bumiputra') == 0){
        echo "<option value='Malay' >Malay</option>";
        echo "<option value='Chinese' >Chinese</option>";
        echo "<option value='India'>India</option>";
        echo "<option value='Bumiputra' selected >Bumiputra</option>";
        echo "<option value='Other' >Other</option>";
    } else{
        echo "<option value='Malay' >Malay</option>";
        echo "<option value='Chinese' >Chinese</option>";
        echo "<option value='India'>India</option>";
        echo "<option value='Bumiputra' >Bumiputra</option>";
        echo "<option value='Other' selected >Other</option>";
    }
}

session_start();

function msg() {
    if (isset($_SESSION['Edit.Msg'])) {
        echo "<script>";
        echo "alert('Successfully updated')";
        echo "</script>";
        unset($_SESSION['Edit.Msg']);
    }
}
?>

<script>
    function getClass()
    {
        // clear the classes drop down list
        var select = document.getElementById("class");
        for (var i = select.options.length - 1; i >= 0; i--)
        {
            select.remove(i);
        }

        var arr = new Array("1", "2", "3", "4", "5", "6");

        var level = document.getElementById("level").value;
        var classes = document.getElementById("class");
        if (level == "Primary") {
            for (var i = 1; i < arr.length + 1; ++i) {
                var option = document.createElement('option');
                option.text = option.value = i;
                classes.add(option, 0);
            }
        }
        else if (level == "Secondary") {
            for (var i = 1; i < arr.length; ++i) {
                var option = document.createElement('option');
                option.text = option.value = i;
                classes.add(option, 0);
            }
        }
    }
</script>

<div id="right">
    <a href="?f=student&loc=searchStudent">Back to Student List</a>
    <h2>Edit Student</h2>

    <form id="editStudentForm" method="post" action="../widget/editStudent_bg.php">
        <h3>Student Particular</h3>
        <div class="fill">
            <table>
                <tr>
                    <td>Student ID</td>
                    <td><input type='text' id='id' name='id' readonly value="<?php echo $row[StudentID]; ?>" /></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><input type="text" id="name" name="name" required autofocus value="<?php echo $row[Name]; ?>" ></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td>
                        <select id="gender" name="gender">
                            <?php getDefaultGender(); ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Race</td>
                    <td>
                        <select id="race" name="race">
                            <?php getDefaultRace(); ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Contact (Home)</td>
                    <td><input type="text" id="home" name="home" value="<?php echo $row[PhoneH]; ?>" ></td>
                </tr>
                <tr>
                    <td>Contact (Mobile)</td>
                    <td><input type="text" id="mobile" name="mobile" value="<?php echo $row[PhoneM]; ?>" ></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><textarea id="address" name="address" form="editStudentForm" rows="5" cols="50" ><?php echo $row[Address]; ?></textarea></td>
                </tr>
                <tr>
                    <td>Level</td>
                    <td>
                        <select id="level" name="level" required onchange="getClass();">
                            <option value=""></option>
                            <?php
                            $level = $row[Level];
                            if (strpos($level, 'Primary') !== false) {
                                echo "<option value='Primary' selected='selected'>Primary</option>";
                            } else {
                                echo "<option value='Primary' >Primary</option>";
                            }

                            if (strpos($level, 'Secondary') !== false) {
                                echo "<option value='Secondary' selected='selected'>Secondary</option>";
                            } else {
                                echo "<option value='Secondary' >Secondary</option>";
                            }

                            if (strpos($level, 'nil') !== false) {
                                echo "<option value='nil' selected='selected'>N/A</option>";
                            } else {
                                echo "<option value='nil' >N/A</option>";
                            }
                            ?>
                        </select>
                        <select id="class" name="class">
                            <?php getDefaultClass(); ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>
                        <select id="status" name="status" >
                            <?php
                            $status = $row[Status];
                            if (strpos($status, 'Active') !== false) {
                                echo "<option value='Active' selected='selected'>Active</option>";
                            } else {
                                echo "<option value='Active' >Active</option>";
                            }

                            if (strpos($status, 'Inactive') !== false) {
                                echo "<option value='Inactive' selected='selected'>Inactive</option>";
                            } else {
                                echo "<option value='Inactive' >Inactive</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>

        <h3>Parent/Guardian Particular</h3>
        <div class="fill">
            <table>
                <tr>
                    <td>Name</td>
                    <td><input type="text" id="parentName" name="parentName" value="<?php echo $row[ParentName]; ?>"></td>
                </tr>
                <tr>
                    <td>Contact (Mobile)</td>
                    <td><input type="text" id="parentPhone" name="parentPhone" value="<?php echo $row[ParentPhone]; ?>"></td>
                </tr>
            </table>
        </div>

        <h3>Subject</h3>
        <div class="fill">
            <table class="fill"  style="width: 70%;">
                <tr>
                    <th class="fill">Primary</th>
                    <th class="fill">Secondary</th>
                    <th class="fill">Other</th>
                </tr>

                <tr>
                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Primary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Secondary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("nil"); ?>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <input type="submit" value="Save" onclick="return confirm('Confirm Save?')" />
    </form>

    <?php msg(); ?>
</div>
