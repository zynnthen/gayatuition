<?php

function querySubject($str) {
    $query = mysql_query("SELECT * FROM Subject Where Type='$str' AND Package IS NULL");

    while ($info = mysql_fetch_assoc($query)) {
        echo "<tr><td>";
        echo "<input type='checkbox' id='{$info['SubjectID']}' name='subject[]' value='{$info['SubjectID']}' >";
        echo $info['Description'];
        echo "</td></ tr>";
    }
}

session_start();
function msg() {
    if (isset($_SESSION['Add.Msg'])) {
        echo "<script>";
        echo "alert('Successfully inserted')";
        echo "</script>";
        unset($_SESSION['Add.Msg']);
    }
}
?>

<div id="right">
    <h2>New Teacher</h2>

    <form id="newTeacherForm" method="post" action="../widget/newTeacher_bg.php">
        <h3>Particular</h3>
        <div class="fill">
            <table>
                <tr>
                    <td>Name</td>
                    <td><input type="text" id="name" name="name" required autofocus></td>
                </tr>
                <tr>
                    <td>Contact (Mobile)</td>
                    <td><input type="text" id="mobile" name="mobile" required></td>
                </tr>
                <tr>
                    <td>Bank</td>
                    <td><input type="text" id="bank" name="bank" required></td>
                </tr>
                <tr>
                    <td>Account No.</td>
                    <td><input type="text" id="accnum" name="accnum" required></td>
                </tr>
            </table>
        </div>

        <h3>Teaching Subject</h3>
        <div class="fill">

            <table class="fill"  style="width: 70%;">
                <tr>
                    <th class="fill">Primary</th>
                    <th class="fill">Secondary</th>
                    <th class="fill">Other</th>
                </tr>

                <tr>
                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Primary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Secondary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("nil"); ?>
                        </table>
                    </td>
                </tr>
            </table>

        </div>

        <input type="submit" value="Save" onclick="return confirm('Confirm Save?')" />
    </form>

    <?php msg(); ?>
</div>
