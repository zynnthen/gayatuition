<?php
if (!isset($_GET['id'])) {
    header("location:home.php?f=teacher&loc=searchTeacher");
}

$teacherid = $_GET['id'];
$query = "SELECT * FROM Teacher WHERE TeacherID = '" . $teacherid . "'  ";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

function querySubject($str) {
    $query = mysql_query("SELECT * FROM Subject Where Type='$str' AND Package IS NULL");

    while ($info = mysql_fetch_assoc($query)) {
        $checked = checkRegSubject($info['SubjectID']);
        echo "<tr><td>";
        echo "<input type='checkbox' id='{$info['SubjectID']}' name='subject[]' value='{$info['SubjectID']}' $checked >";
        echo $info['Description'];
        echo "</td></ tr>";
    }
}

function checkRegSubject($subjectID) {
    $teacher_subject_query = mysql_query("SELECT * FROM TeacherSubject Where TeacherID='" . $_GET['id'] . "' AND SubjectID = $subjectID");
    $num_row = mysql_num_rows($teacher_subject_query);
    if ($num_row == 1) {
        return 'checked';
    }
}

function getDefaultStatus() {
    $query = "SELECT * FROM Teacher WHERE TeacherID = '" . $_GET['id'] . "'  ";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    $status = $row[Status];

    if (strcasecmp($status, 'Working') == 0) {
        echo "<option value='Working' selected >Working</option>";
        echo "<option value='Resign' >Resign</option>";
    } else {
        echo "<option value='Working' >Working</option>";
        echo "<option value='Resign' selected >Resign</option>";
    }
}

session_start();

function msg() {
    if (isset($_SESSION['Edit.Msg'])) {
        echo "<script>";
        echo "alert('Successfully updated')";
        echo "</script>";
        unset($_SESSION['Edit.Msg']);
    }
}
?>

<div id="right">
    <a href="?f=teacher&loc=searchTeacher">Back to Teacher List</a>
    <h2>Edit Teacher</h2>

    <form id="editTeacherForm" method="post" action="../widget/editTeacher_bg.php">
        <h3>Particular</h3>
        <div class="fill">
            <table>
                <tr>
                    <td>Teacher ID</td>
                    <td><input type='text' id='id' name='id' readonly value="<?php echo $row[TeacherID]; ?>" /></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><input type="text" id="name" name="name" required autofocus value="<?php echo $row[Name]; ?>"></td>
                </tr>
                <tr>
                    <td>Contact (Mobile)</td>
                    <td><input type="text" id="mobile" name="mobile" required value="<?php echo $row[Phone]; ?>"></td>
                </tr>
                <tr>
                    <td>Bank</td>
                    <td><input type="text" id="mobile" name="bank" required value="<?php echo $row[Bank]; ?>"></td>
                </tr>
                <tr>
                    <td>Account No.</td>
                    <td><input type="text" id="mobile" name="accnum" required value="<?php echo $row[AccountNo]; ?>"></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>
                        <select id="status" name="status">
                            <?php getDefaultStatus(); ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>

        <h3>Teaching Subject</h3>
        <div class="fill">

            <table class="fill"  style="width: 70%;">
                <tr>
                    <th class="fill">Primary</th>
                    <th class="fill">Secondary</th>
                    <th class="fill">Other</th>
                </tr>

                <tr>
                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Primary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Secondary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("nil"); ?>
                        </table>
                    </td>
                </tr>
            </table>

        </div>

        <input type="submit" value="Save" onclick="return confirm('Confirm Save?')" />
    </form>

    <?php msg(); ?>
</div>
