<!DOCTYPE html>
<?php
session_start();

function errorMsg() {
    if (isset($_SESSION['Login.Error'])) {
        echo "<font color='red'>";
        echo $_SESSION['Login.Error'];
        echo "</font>";
        unset($_SESSION['Login.Error']);
    }
}

function errorUser() {
    if (isset($_SESSION['username'])) {
        echo $_SESSION['username'];
        unset($_SESSION['username']);
    }
}
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pusat Tuisyen Gaya</title>
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
    </head>
    <body>

        <div id="container">
            <h1 style="text-align:center">Pusat Tuisyen Gaya</h1>
            <div id="error"></div>
            <div id="login">
                <form id='loginForm' method="post" action="../widget/loginAction.php">
                    <fieldset >
                        <legend><font size="6">Login</font></legend>
                        <br/>

                        <label>Username : </label>
                        <input type='text' name='username' id='username' required value="<?php errorUser() ?>" />

                        <br/><br/>

                        <label>Password : </label>
                        <input type='password' name='password' id='password' required  />

                        <br/>
                        <?php errorMsg(); ?>
                        <br/>

                        <input type="submit" id="submit" value="Login"/>
                    </fieldset>
                </form>
            </div>

        </div>

    </body>
</html>
